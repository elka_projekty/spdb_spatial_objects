'use strict';

// Declare app level module which depends on views, and components
angular.module('spdb', [
    'ngRoute',
    'spdb.main',
    'spdb.form',
    'spdb.map',
    'spdb.results',
    'spdb.services',
    'ngDialog'
]).
        config(['$routeProvider', function ($routeProvider) {

                $routeProvider
                        .when("/main", {
                            templateUrl: "main_view/main.html",
                            controller: "MainCtrl"
                        })
                        .otherwise({
                            redirectTo: '/main'});
            }]);
'use strict';

angular.module('spdb.form', ['ngRoute'])
        .controller('FormCtrl', ['$scope', '$http', 'SpatialDataService', 'ngDialog', function ($scope, $http, SpatialDataService, ngDialog) {


                //url serwisów
                var overPassApiBaseUrl = 'http://overpass.osm.rambler.ru/cgi/interpreter?data=';
                var yourNavigationBaseUrl = 'http://www.yournavigation.org/api/dev/route.php?';

                //uchwyt do okna ngDialog
                var dialogWindow = null;

                var targetsToCheck;

                /**
                 * Oczekiwanie na zmiane punktu docelowego
                 * i w odpowiedzi wyznaczenie drogi do podanego
                 * punktu docelowego
                 */
                document.addEventListener('target:change', function (e) {
                    getRouteFromGivenCoordinates(
                            SpatialDataService.getSrcPlace().lat,
                            SpatialDataService.getSrcPlace().lon,
                            SpatialDataService.getCurrentTargetPlace().lat,
                            SpatialDataService.getCurrentTargetPlace().lon
                            );
                }, false);

                /**
                 * Funkcja odpowiedzialna za znalezienie trasy pomiedzy dwoma podanymi miejscami
                 * @param {String} sourcePlaceName
                 * @param {String} targetPlaceName
                 * @returns {undefined}
                 */
                $scope.findPlaceByName = function () {

                    var srcElem = {};
                    var urlQuery = parseQueryModelToUrl();

                    serviceOfSearchBegin();

                    if ($scope.isSourcePlaceEnabled) {
                        getRouteFromSrcName(urlQuery);
                    } else {
                        if ($scope.queryModel.srcPlaceLon && $scope.queryModel.srcPlaceLat) {

                            srcElem.lon = $scope.queryModel.srcPlaceLon;
                            srcElem.lat = $scope.queryModel.srcPlaceLat;

                            logStatus('Wyznaczono obiekt z podanych współrzędnych');

                            SpatialDataService.setSrcPlace(srcElem);
                            getTargetElementsForQuery(urlQuery, srcElem);
                        } else {
                            alert('Nie podano współrzędnych');
                        }
                    }
                };

                /**
                 * Funkcja wyszukujaca obiekt startowy po
                 * nazwie
                 * 
                 * @param {type} urlQuery
                 * @returns {undefined}
                 */
                var getRouteFromSrcName = function (urlQuery) {

                    var start = new Date();
                    var srcElem = {};

                    $http.get(overPassApiBaseUrl + '%5Bout:json%5D;node%5Bname%3D"' + $scope.queryModel.srcPlaceName + '"%5D%3Bout%3B').
                            success(function (data, status, headers, config) {
                                console.log("Source request time: ", new Date() - start, "ms");

                                srcElem = data.elements[0] || {};
                                //pointOutMiddleOfStartArea(data.elements, srcElem);

                                logStatus('Wyznaczono obiekt startowy');

                                console.log('srcElem: ', srcElem);

                                SpatialDataService.setSrcPlace(srcElem);
                                getTargetElementsForQuery(urlQuery, srcElem);
                            });
                };

                /**
                 * Funkcja wyznaczajaca srodkowe wspolrzedne z otrzymanego
                 * zbioru punktow przestrzennych
                 * 
                 * @param {type} nodesArray
                 * @param {type} srcElem
                 * @returns {undefined}
                 */
                var pointOutMiddleOfStartArea = function (nodesArray, srcElem) {

                    srcElem = srcElem || {};
                    nodesArray = nodesArray || [];

                    var lonSorted = [];
                    var latSorted = [];

                    nodesArray.forEach(function(item){
                        lonSorted.push(item.lon);
                        latSorted.push(item.lat);
                    });

                    lonSorted = lonSorted.sort();
                    latSorted = latSorted.sort();

                    var midIndex = Math.round(nodesArray.length / 2) - 1;
         
                    srcElem.lon = lonSorted[midIndex];
                    srcElem.lat = latSorted[midIndex];
                };


                /**
                 * Funkcja wyszujujaca obiektow
                 * docelowych woikół obiektu startowego
                 * 
                 * @param {type} urlQuery
                 * @param {type} srcElem
                 * @returns {undefined}
                 */
                var getTargetElementsForQuery = function (urlQuery, srcElem) {

                    var dstElem = {};
                    var start = new Date();

                    $http.get(overPassApiBaseUrl + '%5Bout:json%5D;' + urlQuery).
                            success(function (data, status, headers, config) {
                                console.log("Target request time: ", new Date() - start, "ms");
                                targetsToCheck = data.elements.length;
                                document.addEventListener('target:add', function (e) {
                                    targetsToCheck = targetsToCheck - 1;
                                    if (targetsToCheck == 0) {
                                        document.removeEventListener('target:add');
                                        dstElem = SpatialDataService.getCurrentTargetPlace();
                                        srcElem = SpatialDataService.getSrcPlace();
                                        getRouteFromGivenCoordinates(srcElem.lat, srcElem.lon, dstElem.lat, dstElem.lon);
                                    }
                                }, false);
                                if (($scope.queryModel.searchTravelTime != undefined && $scope.queryModel.searchTravelTime != 0) ||
                                        (getRadiusValue() != undefined && getRadiusValue() == 'navigation')) {
                                    SpatialDataService.setTargetPlacesArray([]);
                                    data.elements.forEach(function (item) {
                                        checkDistanceAndTimeNeeds(item);
                                    });
                                } else
                                {
                                    SpatialDataService.setTargetPlacesArray(data.elements);
                                    dstElem = data.elements[0] || {};

                                    logStatus('Wyznaczono obiekt docelowy');

                                    getRouteFromGivenCoordinates(srcElem.lat, srcElem.lon, dstElem.lat, dstElem.lon);
                                }
                            });
                };

                /**
                 * Funkcja sprawdza czy dane miejsce docelowe
                 * spełnia warunki długości drogi i czasu dojazdu
                 * 
                 * @param {type} item
                 * @returns {undefined}
                 */
                var checkDistanceAndTimeNeeds = function (item)
                {
                    var srcElem = SpatialDataService.getSrcPlace();
                    var start = new Date();
                    $http.get(yourNavigationBaseUrl + 'flat=' + srcElem.lat + '&flon=' + srcElem.lon + '&tlat=' + item.lat + '&tlon=' + item.lon + '&v=motorcar&fast=1&layer=mapnik&instructions=1&format=geojson').
                            success(function (data, status, headers, config) {
                                console.log("Navigation request time: ", new Date() - start, "ms");

                                var distance = SpatialDataService.calculateWalkDistance(srcElem, data.coordinates[0]);
                                var wholedistance = +data.properties.distance;
                                var wholetime = 0;
                                if (data.properties.traveltime.length == 4)
                                    wholetime = Math.round(+data.properties.traveltime / 100);


                                var wholetime = Math.round(+data.properties.traveltime / 100);
                                wholedistance += distance / 1000;
                                wholetime += SpatialDataService.calculateWalkTime(distance);

                                distance = SpatialDataService.calculateWalkDistance(item, data.coordinates[data.coordinates.length - 1]);
                                wholedistance += distance / 1000;
                                wholetime += SpatialDataService.calculateWalkTime(distance);

                                var canAdd = true;
                                if ($scope.queryModel.searchTravelTime != undefined &&
                                        $scope.queryModel.searchTravelTime != 0 &&
                                        wholetime > $scope.queryModel.searchTravelTime) {
                                    canAdd = false;
                                }

                                if (getRadiusValue() != undefined &&
                                        getRadiusValue() == 'navigation' &&
                                        wholedistance > $scope.queryModel.searchAreaRadius) {
                                    canAdd = false;
                                }

                                if (canAdd) {
                                    var targetPlaceArray = SpatialDataService.getTargetPlacesArray();
                                    targetPlaceArray.push(item);
                                    if (targetPlaceArray.length == 1)
                                        SpatialDataService.setCurrentTargetPlace(item);

                                }
                                //do testów:
                                var srcCoordinates = ol.proj.transform([+srcElem.lon, +srcElem.lat], 'EPSG:4326', 'EPSG:3857');
                                var targetCoordinates = ol.proj.transform([+item.lon, +item.lat], 'EPSG:4326', 'EPSG:3857');
                                var testDistance = Math.sqrt(Math.pow(srcCoordinates[0] - targetCoordinates[0], 2) + Math.pow(srcCoordinates[1] - targetCoordinates[1], 2));
                                console.log("lon", item.lon);
                                console.log("lat", item.lat);
                                console.log("Straight distance: ", testDistance / 1000);
                                console.log("Navigation distance:", wholedistance);
                                console.log("Navigation traveltime:", wholetime);

                                var event = new Event('target:add', true, true);
                                document.dispatchEvent(event);

                            }).
                            error(function (data, status, headers, config) {
                            });
                };

                /**
                 * Funkcja wykonujaca poboczne funkcjonalnosci:
                 * otworzenie pop-upu, rozpoczecie licznika
                 * @returns {undefined}
                 */
                var serviceOfSearchBegin = function () {

                    $scope.statusText = [];
                    $scope.secondsElapsed = 0;

                    dialogWindow = ngDialog.open({
                        template: 'dialogs_templates/waiting-window.html',
                        plain: false,
                        scope: $scope
                    });

                    $scope.statusText.push('Rozpoczęcie szukania');
                    var timer = setInterval(
                            function () {
                                $scope.secondsElapsed++;
                                $scope.$apply();
                            }, 1000
                            );
                };

                /**
                 * Funkcja zwracająca drogę dla podanych współrzędnych
                 * @param {Int} lat
                 * @param {Int} lon
                 * @returns {undefined}
                 */
                var getRouteFromGivenCoordinates = function (flat, flon, tlat, tlon) {
                    var start = new Date();
                    $http.get(yourNavigationBaseUrl + 'flat=' + flat + '&flon=' + flon + '&tlat=' + tlat + '&tlon=' + tlon + '&v=motorcar&fast=1&layer=mapnik&instructions=1&format=geojson').
                            success(function (data, status, headers, config) {
                                console.log("Navigation request time: ", new Date() - start, "ms");
                                logStatus('Wyznaczono droge');
                                SpatialDataService.setGeoJSONRoute(data);

                                dialogWindow.close();
                                //wystrzelenie eventu informujacego o zakonczeniu przetwarzania
                                dispatchEndOfSearchingEvent();
                            }).
                            error(function (data, status, headers, config) {
                            });
                };

                /**
                 * Kolekcja typów wyszkuwianych miejsc
                 */
                $scope.objectsTypeToSearch =
                        [
                            'node',
                            'way',
                            'relation'
                        ];

                /**
                 * Hash Map zawierająca mapowania klucz - dostępne wartości
                 */
                $scope.keyValueHashMap =
                        {
                            amenity: ['fire_station', 'theatre', 'fuel', 'clock', 'library', 'cafe', 'fast_food', 'drinking_water', 'restaurant', 'bureau_de_change', 'bar', 'bicycle_parking', 'telephone', 'bank', 'nightclub', 'parking', 'fountain', 'pharmacy', 'toilets', 'atm', 'bench', 'taxi', 'shower', 'place_of_worship', 'post_office', 'cinema'],
                            highway: ['bus_stop', 'motorway_junction', 'primary', 'secondary', 'tertiary', 'track', 'residential', 'traffic_signals', 'crossing', 'milestone', 'street_lamp'],
                            place: ['suburb', 'city', 'village', 'locality', 'hamlet', 'town', 'neighbourhood', 'school', 'pub', 'alcohol'],
                            population: ['duzo', 'malo'],
                            railway: ['station'],
                            public_transport: ['stop_position'],
                            is_in: ['Polska', 'Deutschland'],
                            oneway: ['yes', 'no'],
                            lanes: ['1', '2', '3'],
                            bicycle: ['yes', 'no'],
                            lit: ['yes', 'no'],
                            maxspeed: ['50', '70'],
                            surface: ['asphalt'],
                            traffic_sign: ['city_limit'],
                            crossing: ['uncontrolled'],
                            shop: ['carpet', 'car_repair', 'convenience', 'deli', 'hairdresser', 'greengrocer', 'books', 'supermarket', 'travel_agency', 'chemist', 'dry_cleaning', 'kiosk'],
                            historic: ['monument', 'memorial'],
                            tourism: ['viewpoint', 'museum', 'information', 'artwork', 'camp_site'],
                            tram: ['yes', 'no'],
                            cuisine: ['regional', 'burger', 'mexican', 'polish', 'american', 'kebab', 'thai', 'pizza', 'fish'],
                            atm: ['yes', 'no'],
                            natural: ['tree', 'peak', 'spring', 'scree', 'saddle', 'birds_nest', 'cave_entrance', 'beach']
                        };

                /**
                 * Kolekcja wybrancyh krotek  klucz-wartość
                 */
                $scope.keyValueTupleArray = [];

                /**
                 * Kolekcja danych wartości dopasowaynch do klucza
                 */
                $scope.valuesArrayToDisplay = [];

                /**
                 * Funkcja parsujaca obiekt tworzony w formularzu na url, ktory zostanie strzelony w OverpassAPI
                 * @returns {form_L12.parseQueryModelToUrl.urlQuery}
                 */
                var parseQueryModelToUrl = function () {

                    var queryString = '';

                    //parsowanie miejsca źródłowego
                    if ($scope.isSourcePlaceEnabled) {

                        queryString += 'node["name"="' + $scope.queryModel.srcPlaceName + '"];';

                    } else if ($scope.queryModel.srcPlaceLon && $scope.queryModel.srcPlaceLat) {

                        //bb box centralnie wokól miejsca
                        queryString += 'node(' +
                                (0.999999 * $scope.queryModel.srcPlaceLat).toFixed(6) + ',' +
                                (0.999999 * $scope.queryModel.srcPlaceLon).toFixed(6) + ',' +
                                (1.000001 * $scope.queryModel.srcPlaceLat).toFixed(6) + ',' +
                                (1.000001 * $scope.queryModel.srcPlaceLon).toFixed(6) + ');';
                    }

                    //domyslnie node
                    if (!$scope.queryModel.targetObjectType) {
                        $scope.queryModel.targetObjectType = 'node';
                    }

                    queryString += $scope.queryModel.targetObjectType;

                    //parsowanie miejsc docelowych
                    if (!$scope.queryModel.searchAreaRadius) {
                        $scope.queryModel.searchAreaRadius = 10000; //10km
                    }
                    queryString += '(around:' + 1000 * $scope.queryModel.searchAreaRadius + ')';

                    if ($scope.queryModel.isTargetRegxpName && $scope.queryModel.tgtPlaceName) {

                        queryString += '["name"~"' + $scope.queryModel.tgtPlaceName + '"]';

                    } else if (!$scope.queryModel.isTargetRegxpName && $scope.queryModel.tgtPlaceName) {

                        queryString += '["name"="' + $scope.queryModel.tgtPlaceName + '"]';
                    }

                    if ($scope.keyValueTupleArray.length > 0) {
                        $scope.keyValueTupleArray.forEach(function (item) {
                            queryString += '["' + item.key + '"="' + item.value + '"]';
                        });
                    }

                    queryString += ';out body;';

                    console.log('queryString: ', queryString);

                    var urlQuery = encodeURI(queryString);
                    return urlQuery;
                };

                /**
                 * Funkcja pobierająca parę klucz-wartość z selectów
                 * sprawdza czy dany klucz już został użyty i jak nie
                 * to wsadza go do kolekcji
                 * @returns {undefined}
                 */
                $scope.addKeyValueTuple = function () {

                    var isAvaible = $scope.keyValueTupleArray.some(function (item) {
                        return item.key === $scope.queryModel.objectKeyToSearch;
                    });

                    if (!isAvaible) {
                        $scope.keyValueTupleArray.push({
                            key: $scope.queryModel.objectKeyToSearch,
                            value: $scope.queryModel.objectValueToSearch
                        });
                    }
                };

                /**
                 * Funkcja obsługująca wgranie nowych wartości do selecta wartości
                 * po wybraniu nowej klucza
                 * @returns {undefined}
                 */
                $scope.loadNewValuesSet = function () {
                    $scope.valuesArrayToDisplay = $scope.keyValueHashMap[$scope.queryModel.objectKeyToSearch];
                };

                /**
                 * Funkcja usuwająca z tabeli zaznaczony wiersz
                 * @returns {undefined}
                 */
                $scope.removeKeyValueTuple = function () {

                    var elemToRemove = $scope.keyValueTupleArray.filter(function (item) {
                        return item.key === $scope.keySelected;
                    });

                    if (elemToRemove.length > 0) {
                        var indexToRemove = $scope.keyValueTupleArray.indexOf(elemToRemove[0]);
                        $scope.keyValueTupleArray.splice(indexToRemove, 1);
                        $scope.keySelected = '';
                    }
                };

                /**
                 * Aktualnie zaznaczony wiersz (wedlug klucza)
                 */
                $scope.keySelected = '';

                /**
                 * Funkcja podświetlająca kliknięty wiersz 
                 * w tabeli oraz zapisanie, który został 
                 * kliknięty w razie potrzeby jego usunięcia
                 * 
                 * @param {type} selectedKey
                 * @returns {undefined}
                 */
                $scope.setSelected = function (selectedKey) {

                    $scope.keySelected = selectedKey;
                };

                /**
                 * Zmienna do wyswietlania informacji w pup-upie
                 */
                $scope.statusText = [];

                /**
                 * Funkcja obslugujaca logowanie na popupie
                 * 
                 * @param {type} status
                 * @returns {undefined}
                 */
                var logStatus = function (status) {

                    $scope.statusText.push(status);
                };

                /**
                 * Zmienna do trzymania czasu wykonywania zapytania
                 */
                $scope.secondsElapsed = 0;


                /**
                 * Funkcja ustalająca tryb wyznaczania
                 * obiektu źródłowego
                 * 
                 * @param {type} mode
                 * @returns {undefined}
                 */
                $scope.setEnabledMode = function (mode) {

                    if (mode === 'srcPlaceRadio') {
                        $scope.isSourcePlaceEnabled = true;
                        $scope.isSourceCoordsEnabled = false;

                        $scope.queryModel.srcPlaceLat = '';
                        $scope.queryModel.srcPlaceLon = '';
                    } else if (mode === 'srcPlaceCoordsRadio') {
                        $scope.isSourcePlaceEnabled = false;
                        $scope.isSourceCoordsEnabled = true;
                    } else if (mode === 'srcActualPositionRadio') {
                        $scope.isSourcePlaceEnabled = false;
                        $scope.isSourceCoordsEnabled = false;
                        setLocationParameters();
                    }
                };

                /**
                 * Funkcja pobierajaca lokalizacje z przegladarki (za pozwoleniem użytkownika)
                 * @returns {undefined}
                 */
                var setLocationParameters = function () {
                    if (navigator.geolocation) {

                        var handlePosition = function (position) {
                            $scope.queryModel.srcPlaceName = '';

                            var lon = position.coords.longitude.toFixed(6);
                            var lat = position.coords.latitude.toFixed(6);

                            $scope.queryModel.srcPlaceLat = lat;
                            $scope.queryModel.srcPlaceLon = lon;
                        };
                        navigator.geolocation.getCurrentPosition(handlePosition);

                    } else {
                        window.alert("Geolocation is not supported by this browser.");
                    }
                };


                /**
                 * Flagi do oznaczania trybu wyznaczania
                 * obiektu źródłowego
                 */
                $scope.isSourcePlaceEnabled = true;
                $scope.isSourceCoordsEnabled = false;

                /**
                 * Funkja inicjalizująca model
                 * @returns {undefined}
                 */
                $scope.initQueryModel = function () {

                    $scope.queryModel = $scope.queryModel || {};
                    $scope.queryModel.srcPlaceName = 'Warszawa';
                    $scope.queryModel.tgtPlaceName = 'Radom';
                    $scope.queryModel.targetObjectType = 'node';
                    $scope.queryModel.objectKeyToSearch = 'place';
                    $scope.queryModel.objectValueToSearch = 'suburb';
                    $scope.queryModel.searchAreaRadius = 120;

                };

                //inicjalizacja modelu
                $scope.initQueryModel();

                /**
                 * Funkcja informujaca o zakonczeniu szukania sciezek
                 * 
                 * @returns {undefined}
                 */
                var dispatchEndOfSearchingEvent = function () {

                    var event = new Event('route:found', true, true);
                    document.dispatchEvent(event);
                };

                /**
                 * Funkcja do pobierania wartości radio buttona
                 * określającego czy podany dystans miejsca docelowego
                 * jest odległością w linii prostej czy długością
                 * drogi do przebycia.
                 */
                var getRadiusValue = function () {
                    var radios = document.getElementsByName('radius');
                    var value;
                    for (var i = 0, length = radios.length; i < length; i++) {
                        if (radios[i].checked) {
                            value = radios[i].value;
                            break;
                        }
                    }
                    return value;
                };
            }]);
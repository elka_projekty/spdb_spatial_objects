'use strict';

angular.module('spdb.map', ['ngRoute'])
        .controller('MapCtrl', ['$scope', 'SpatialDataService', function ($scope, SpatialDataService) {

                document.addEventListener('route:found', function (e) {
                    init();
                }, false);

                /**                
                 * Funkcja przekształcająca współrzęden z sytemu
                 * EPSG:4326 na EPSG:3857
                 * @param {type} lon
                 * @param {type} lat
                 * @returns {unresolved}
                 */
                var matchCoords = function (lon, lat) {

                    if (lon && lat)
                        return ol.proj.transform([+lon, +lat], 'EPSG:4326', 'EPSG:3857');
                    else
                        return ol.proj.transform([0, 0], 'EPSG:4326', 'EPSG:3857');
                };


                function init() {

                    markSrcLocalization();
                    if (markTargetLocalizations()) {
                        markCurrentTargetLocalization();
                        markPathToTarget();
                        markFoodRoadPath();
                    }
                }
                ;

                //punkt startowy
				/**
				* obraz dla punktu startowego
				*/
                var image = new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
                    anchor: [0.5, 24],
                    anchorXUnits: 'fraction',
                    anchorYUnits: 'pixels',
                    opacity: 0.95,
                    src: 'icon/red_pin.png'
                }));
				
				/**
				* styl dla punktu startowego i dla trasy
				*/
                var styles = {
                    'Point': [new ol.style.Style({
                            image: image
                        })],
                    'LineString': [new ol.style.Style({
                            stroke: new ol.style.Stroke({
                                color: 'violet',
                                width: 2
                            })
                        })]
                };
				
				/**
				* Metoda do pobrania odpowiedniego sytlu dla punktu startowego lub dla trasy
				*/
                var styleFunction = function (feature, resolution) {
                    return styles[feature.getGeometry().getType()];
                };

                var vectorSource = new ol.source.Vector();

                var vectorLayer = new ol.layer.Vector({
                    source: vectorSource,
                    style: styleFunction
                });

                //cel
				/**
				* styl dla punktu docelowego
				*/
                var targetStyles = {
                    'Point': [new ol.style.Style({
                            image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
                                anchor: [0.5, 24],
                                anchorXUnits: 'fraction',
                                anchorYUnits: 'pixels',
                                opacity: 0.95,
                                src: 'icon/blue_pin.png'
                            }))
                        })]
                }

				/**
				* Metoda do pobrania odpowiedniego sytlu dla punktu docelowego
				*/
                var targetStyleFunction = function (feature, resolution) {
                    return targetStyles[feature.getGeometry().getType()];
                };

                var targetVectorSource = new ol.source.Vector();

                var targetVectorLayer = new ol.layer.Vector({
                    source: targetVectorSource,
                    style: targetStyleFunction
                });


                //aktualny cel
				/**
				* styl dla aktualnie wybranego punktu docelowego
				*/
                var currentTargetStyles = {
                    'Point': [new ol.style.Style({
                            image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
                                anchor: [0.5, 24],
                                anchorXUnits: 'fraction',
                                anchorYUnits: 'pixels',
                                opacity: 0.95,
                                src: 'icon/green_pin.png'
                            }))
                        })]
                }

				/**
				* Metoda do pobrania odpowiedniego sytlu dla aktualnie wybranego punktu docelowego
				*/
                var currTargetStyleFunction = function (feature, resolution) {
                    return currentTargetStyles[feature.getGeometry().getType()];
                };

                var currTargetVectorSource = new ol.source.Vector();

                var currTargetVectorLayer = new ol.layer.Vector({
                    source: currTargetVectorSource,
                    style: currTargetStyleFunction
                });

                //piesza droga
				/**
				* styl dla drogi pokonywanej pieszo
				*/
                var footRoadStyles = {
                    'LineString': [new ol.style.Style({
                            stroke: new ol.style.Stroke({
                                color: 'gray',
                                width: 1
                            })
                        })]
                };

				/**
				* Metoda do pobrania odpowiedniego sytlu dla drogi pokonywanej pieszo
				*/
                var footRoadStyleFunction = function (feature, resolution) {
                    return footRoadStyles[feature.getGeometry().getType()];
                };

                var footRoadVectorSource = new ol.source.Vector();

                var footRoadVectorLayer = new ol.layer.Vector({
                    source: footRoadVectorSource,
                    style: footRoadStyleFunction
                });

                var map = new ol.Map({
                    layers: [
                        new ol.layer.Tile({
                            source: new ol.source.OSM()
                        }),
                        vectorLayer,
                        targetVectorLayer,
                        currTargetVectorLayer,
                        footRoadVectorLayer
                    ],
                    target: 'map',
                    controls: ol.control.defaults({
                        attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
                            collapsible: false
                        })
                    }),
                    view: new ol.View({
                        center: matchCoords(20.967467, 52.202018),
                        zoom: 6
                    })
                });
				
				/*
				var exampleLoc = ol.proj.transform(
					[21.0100995, 52.2311676], 'EPSG:4326', 'EPSG:3857');
				map.on('postcompose', function(evt) {
				  evt.vectorContext.setFillStrokeStyle(
					  new ol.style.Fill({color: 'rgba(255, 0, 0, .1)'}),
					  new ol.style.Stroke({color: 'rgba(255, 0, 0, .8)', width: 3}));
				  evt.vectorContext.drawCircleGeometry(
					  new ol.geom.Circle(exampleLoc, 1660));
				});
				*/
                
				/**
				* Funkcja do oznaczania punktu startowego na mapie
				*/
                var markSrcLocalization = function () {
                    vectorSource.clear();
                    markLocalization(SpatialDataService.getSrcPlace(), vectorSource);

                    map.getView().setCenter(matchCoords(SpatialDataService.getSrcPlace().lon, SpatialDataService.getSrcPlace().lat));
                    map.getView().setZoom(15);
                };

				/**
				* Funkcja do oznaczania punktów docelowych na mapie
				*/
                var markTargetLocalizations = function () {
                    targetVectorSource.clear();
                    var localizations = SpatialDataService.getTargetPlacesArray();

                    if (localizations.length === 0) {
                        alert('Nie znaleziono obiektow o podanych kryteriach');
                        return false;
                    }

                    for (var idx = 0; idx < localizations.length; idx++) {
                        if (localizations[idx] != SpatialDataService.getCurrentTargetPlace())
                            markLocalization(localizations[idx], targetVectorSource);
                    }

                    return true;
                };

				/**
				* Funkcja do oznaczania aktualnie wybranego punktu docelowego na mapie
				*/
                var markCurrentTargetLocalization = function () {
                    currTargetVectorSource.clear();
                    if (SpatialDataService.getCurrentTargetPlace() != undefined && SpatialDataService.getTargetPlacesArray() != undefined)
                    {
                        markLocalization(SpatialDataService.getCurrentTargetPlace(), currTargetVectorSource);
                    }
                };

				/**
				* Funkcja do oznaczania punktu na mapie
				*/
                var markLocalization = function (localization, vSource) {
                    var x = localization.lon;
                    var y = localization.lat;
                    vSource.addFeature(new ol.Feature(new ol.geom.Point(matchCoords(x, y))));

                };

				/**
				* Funkcja do oznaczania trasy na mapie
				*/
                var markPathToTarget = function () {
                    //dane do wyswietlenia na mapie
                    var geoJsonRouteObject = SpatialDataService.getGeoJSONRoute();
                    var pathVector = [];

                    geoJsonRouteObject.coordinates.forEach(function (item) {

                        var x = item[0];
                        var y = item[1];

                        pathVector.push(matchCoords(x, y));
                    });
                    var lineFeature = new ol.Feature(new ol.geom.LineString(pathVector));
                    vectorSource.addFeature(lineFeature);
                };

				/**
				* Funkcja do oznaczania na mapie trasy do pokonania pieszo
				*/
                var markFoodRoadPath = function () {
                    footRoadVectorSource.clear();
                    var geoJsonRouteObject = SpatialDataService.getGeoJSONRoute();

                    var srcToRoadVector = [];
                    var item = geoJsonRouteObject.coordinates[0];
                    srcToRoadVector.push(matchCoords(SpatialDataService.getSrcPlace().lon, SpatialDataService.getSrcPlace().lat));
                    srcToRoadVector.push(matchCoords(item[0], item[1]));
                    footRoadVectorSource.addFeature(new ol.Feature(new ol.geom.LineString(srcToRoadVector)));

                    var roadToTargetVector = [];
                    roadToTargetVector.push(matchCoords(SpatialDataService.getCurrentTargetPlace().lon, SpatialDataService.getCurrentTargetPlace().lat));
                    item = geoJsonRouteObject.coordinates[geoJsonRouteObject.coordinates.length - 1];
                    roadToTargetVector.push(matchCoords(item[0], item[1]));
                    footRoadVectorSource.addFeature(new ol.Feature(new ol.geom.LineString(roadToTargetVector)));
                };
            }]);

'use strict';

angular.module('spdb.results', ['ngRoute'])
        .controller('ResultsCtrl', ['$scope', 'SpatialDataService', function ($scope, SpatialDataService) {

                /**
                 * Oblsuga eventu o zakonczeniu poszukiwania sciezek
                 */
                document.addEventListener('route:found', function (e) {
                    init();
                }, false);

                /**
                 * Funkcja inicjujaca kontroler
                 * 
                 * @returns {undefined}
                 */
                var init = function () {

                    $scope.targetPlacesArray = SpatialDataService.getTargetPlacesArray() || [];
					$scope.resultAreaText = SpatialDataService.getCurrentTargetPlace();
					addPathDataToResultArea();
                };


                $scope.keySelectred = 0;

                /**
                 * Funkcja oblsugujaca klikniecie w wiersz z wynikiem
                 * 
                 * @param {type} result
                 * @returns {undefined}
                 */
                $scope.showSelectedResult = function (result) {
                   
                   $scope.keySelectred = result.id;
                   
                    dispatchResultSelectetEvent(result);
					SpatialDataService.setCurrentTargetPlace(result);
					var event = new Event('target:change', true, true);
                    document.dispatchEvent(event);
                };

                /**
                 * obiekt do wyswietlenia w raporcie
                 */
                $scope.resultAreaText = {};


                /**
                 * Funkcja wysylajaca event o klinkietym wierszu w tabeli wynikow
                 * 
                 * @param {type} object
                 * @returns {undefined}
                 */
                var dispatchResultSelectetEvent = function (object) {

                    var event = new CustomEvent('route:selected',{
                        detail : object
                    });
                    document.dispatchEvent(event);
                };
				
				/**
				* Funckja odpowiedzialna za dodanie informacji o ścieżce do podstawowych informacji punktu
				* docelowego wyświetlanych w danych szczegółowych.
				*/
				var addPathDataToResultArea = function(){
					var src = SpatialDataService.getSrcPlace();
					var target = SpatialDataService.getCurrentTargetPlace();
					var geoJsonRouteObject = SpatialDataService.getGeoJSONRoute();
					
					var wholetime = 0;
					if (geoJsonRouteObject.properties.traveltime.length == 4)
						wholetime =  Math.round(+geoJsonRouteObject.properties.traveltime/100);
								
					var distance = SpatialDataService.calculateWalkDistance(src, geoJsonRouteObject.coordinates[0]);
					$scope.resultAreaText["road:walk on begining [meters]"] = distance;
					$scope.resultAreaText["road:walk on begining time [minutes]"] = SpatialDataService.calculateWalkTime(distance);
					wholetime += SpatialDataService.calculateWalkTime(distance);
					$scope.resultAreaText["road:road length [kilometers]"] = geoJsonRouteObject.properties.distance;
					$scope.resultAreaText["road:road traveltime [minutes]"] =  Math.round(+geoJsonRouteObject.properties.traveltime/100);
					distance = SpatialDataService.calculateWalkDistance(target, geoJsonRouteObject.coordinates[geoJsonRouteObject.coordinates.length-1]);
					$scope.resultAreaText["road:walk on end [meters]"] = distance
					$scope.resultAreaText["road:walk on end time [minutes]"] = SpatialDataService.calculateWalkTime(distance);
					wholetime += SpatialDataService.calculateWalkTime(distance);
					$scope.resultAreaText["road:road whole traveltime [minutes]"] = wholetime;
				};
                
                /**
                 * Funkcja filtrujaca wyniki - powoduje
                 * rozbicie obiektu tags na skladowe
                 * 
                 * @param {type} toFilter
                 * @returns {Boolean}
                 */
                $scope.filterTableResults = function(toFilter) {
                                      
                    if(toFilter.key === 'tags'){
                        return false;
                    }                    
                    return true;
                };
            }]);
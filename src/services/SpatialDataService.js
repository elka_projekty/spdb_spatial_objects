'use strict';
var myModule = angular.module('spdb.services', []);
myModule.factory('SpatialDataService', function () {
    var nodesArray = [];
    var srcPlace = {};
	var currentTargetPlace = {};
    var targetPlacesArray = [];
    this.geoJSONRoute = {};

    var setNodesArray = function (newNodesArray) {

        nodesArray = newNodesArray;
    };

    var setTargetPlacesArray = function (newTargetPlacesArray) {
		if (newTargetPlacesArray != undefined && newTargetPlacesArray.length > 0)
			this.currentTargetPlace=newTargetPlacesArray[0];
		else
			this.currentTargetPlace = {};
		
        targetPlacesArray = newTargetPlacesArray;
    };

    var setSrcPlace = function (src) {

        srcPlace = src;
    };

    var getTargetPlacesArray = function () {

        return targetPlacesArray;
    };

    var getSrcPlace = function () {

        return srcPlace;
    };

    var setGeoJSONRoute = function (route) {

        this.geoJSONRoute = route;        
    };

    var getGeoJSONRoute = function () {

        return this.geoJSONRoute;
    };
	
	var setCurrentTargetPlace = function(targetPlace) {
		this.currentTargetPlace = targetPlace;
	};
	
	var getCurrentTargetPlace = function() {
		return this.currentTargetPlace;
	};
	
	var calculateWalkDistance = function(src, targetCoor){
		var srcCoordinates = ol.proj.transform([+src.lon, +src.lat], 'EPSG:4326', 'EPSG:3857');
		var targetCoordinates = ol.proj.transform([+targetCoor[0], +targetCoor[1]], 'EPSG:4326', 'EPSG:3857');
		var distance = Math.sqrt(Math.pow(srcCoordinates[0]-targetCoordinates[0],2)+Math.pow(srcCoordinates[1]-targetCoordinates[1],2));
		return Math.round(distance * 100) / 100;
	};
	
	var calculateWalkTime = function(distance){
		var time = distance*60/3500;
		return Math.round(time);
	};

    return {
        setNodesArray: setNodesArray,
        setSrcPlace: setSrcPlace,
        setTargetPlacesArray: setTargetPlacesArray,
        getSrcPlace: getSrcPlace,
        getTargetPlacesArray: getTargetPlacesArray,
        setGeoJSONRoute: setGeoJSONRoute,
        getGeoJSONRoute: getGeoJSONRoute,
		setCurrentTargetPlace: setCurrentTargetPlace,
		getCurrentTargetPlace: getCurrentTargetPlace,
		calculateWalkDistance: calculateWalkDistance,
		calculateWalkTime: calculateWalkTime
    };
});